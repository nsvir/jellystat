package fr.nsvir.jellystat

import cats.effect.{IO, _}
import fr.nsvir.jellystat.controller.MovieController
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder

import java.net.InetSocketAddress
import scala.concurrent.ExecutionContext.global

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    val services = MovieController.service
    val httpApp = Router("/" -> MovieController.service, "/api" -> services).orNotFound

    BlazeServerBuilder[IO](global)
      .bindSocketAddress(new InetSocketAddress(6868))
      .withHttpApp(httpApp)
      .serve
      .compile
      .drain
      .as(ExitCode.Success)
  }
}
