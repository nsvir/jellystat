package fr.nsvir.jellystat.api

import cats.effect.{IO, _}
import cats.syntax.all._
import fr.nsvir.jellystat.api.model.{Movie, User}
import io.circe._
import io.circe.generic.auto._
import org.http4s.Method._
import org.http4s.{Request, _}
import org.http4s.circe._
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.dsl.io._

import scala.concurrent.ExecutionContext.global

object MovieApi {

  private val jellyUrl = sys.env("JELLY_URL")

  private val jellyToken = sys.env("JELLY_TOKEN")

  implicit private val contextShift: ContextShift[IO] = IO.contextShift(global)

  private def http[T]: (Client[IO] => IO[T]) => IO[T] = BlazeClientBuilder[IO](global).resource.use _

  private val getUsers = GET(
    Uri.unsafeFromString(s"$jellyUrl/users"),
    Header("X-MediaBrowser-Token", jellyToken)
  )

  private def getMovies(userId: String): IO[Request[IO]] = {

    val uri = Uri
      .unsafeFromString(s"$jellyUrl/users/$userId/Items")
      .withQueryParams(Map(
        "Recursive" -> "true",
        "IncludeItemTypes" -> "Movie",
        "Fields" -> "DateCreated"
      ))

    GET(uri, Header("X-MediaBrowser-Token", jellyToken))
  }

  def movies(): IO[Vector[Movie]] = {
    http { client =>
      val users = client.expect(getUsers)(jsonOf[IO, Vector[User]])

      users.flatMap(_.map { user =>

        client.expect[Json](getMovies(user.Id)).map(_.asObject
          .get("Items")
          .flatMap(_.as[Vector[Movie]].toOption)
          .getOrElse(Vector.empty[Movie]))

      }.parSequence).map(_.flatten)
    }
  }
}
