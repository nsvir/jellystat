package fr.nsvir.jellystat.api.model

case class UserData(LastPlayedDate: Option[String], Played: Boolean)
