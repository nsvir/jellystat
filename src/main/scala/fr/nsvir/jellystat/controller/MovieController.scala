package fr.nsvir.jellystat.controller

import cats.effect.IO
import fr.nsvir.jellystat.api.MovieApi
import fr.nsvir.jellystat.api.model.Movie
import fr.nsvir.jellystat.model.MovieStat
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.io._
import org.slf4j.{Logger, LoggerFactory}


object MovieController {

  val log: Logger = LoggerFactory.getLogger(getClass)

  object Sort extends OptionalQueryParamDecoderMatcher[String]("sort")

  object Order extends OptionalQueryParamDecoderMatcher[String]("order")

  implicit class VectorEnrich[T](vector: Vector[T]) {
    def order(mode: Option[String]): Vector[T] = {
      mode match {
        case Some("asc") => vector
        case Some("desc") => vector.reverse
        case _ => vector
      }
    }
  }

  val service: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case request@GET -> Root :? Sort(column) +& Order(mode) =>
      IO(log.info(s"request : ${request.method}, ${request.uri}")) *> Ok(
        MovieController.movies().map(_
          .sortWith(MovieStat.sort(column))
          .order(mode)
          .asJson
        ))
  }

  private def movieToStat(movie: Movie): MovieStat = {
    val played = if (movie.UserData.Played) 1 else 0
    val timestamp = movie.UserData.LastPlayedDate
    MovieStat(movie.Id, movie.Name, movie.DateCreated, played, timestamp)
  }

  private lazy val movieStatReducer: (MovieStat, MovieStat) => MovieStat = {
    case (ms1, ms2) =>
      val lastTimestamp: Option[String] = Seq(ms1.lastPlayed, ms2.lastPlayed).last
      MovieStat(ms1.id, ms1.name, ms1.dateCreated, ms1.played + ms2.played, lastTimestamp)
  }


  def movies(): IO[Vector[MovieStat]] = {
    MovieApi.movies().map { movies =>

      movies
        .groupMapReduce(_.Id)(movieToStat)(movieStatReducer)
        .map { case (_, movieStat: MovieStat) => movieStat }
        .toVector
    }
  }
}
