package fr.nsvir.jellystat.model
import scala.math.Ordering.Implicits._

case class MovieStat(id: String, name: String, dateCreated: String, played: Int, lastPlayed: Option[String])

object MovieStat {

  def sort(maybeSortColumn: Option[String])(a: MovieStat, b: MovieStat): Boolean = {
    maybeSortColumn match {
      case Some("name") => a.name < b.name
      case Some("id") => a.id < b.id
      case Some("dateCreated") => a.dateCreated < b.dateCreated
      case Some("played") => a.played < b.played
      case Some("lastPlayed") => a.lastPlayed < b.lastPlayed
      case _ => a.name < b.name
    }
  }

}