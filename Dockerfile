FROM openjdk:8-jre
MAINTAINER Nicolas SVIRCHEVSKY <n.svirchevsky@gmail.com>

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /jellystat
ENTRYPOINT ["java", "-jar", "/usr/share/jellystat/jellystat.jar"]
EXPOSE 6868

ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/jellystat/jellystat.jar
